# Products Search

<p align="center">
    <a href="https://www.ruby-lang.org/en/">
    <img src="https://img.shields.io/badge/Ruby-v2.6.3-green.svg" alt="ruby version">
  </a>
  <a href="http://rubyonrails.org/">
    <img src="https://img.shields.io/badge/Rails-v6.0.2-brightgreen.svg" alt="rails version">
  </a>
</p>

*Products Search* is a demo search app built with Ruby on Rails 6.0.2. It uses
[ElasticSearch](https://www.elastic.co/products/elasticsearch) for indexing data
and the lovely [Searchkick](https://github.com/ankane/searchkick) gem for
searching data.

## Features

- [x] Import products from JSON file
- [x] Search products by title, description, tags, price and origin country
- [x] Display product details in a modal
- [x] Filter results by origin country
- [x] Sort results by relevance, newest, lowest price, highest price, etc

## Next Steps

Here are a list of features and improvements that will eventually be addressed:

- [x] Partial match of keywords
- [x] Paginate search results
- [ ] Localize strings and specs
- [ ] Caching views layer
- [ ] Set up load test with a few hundred records

## Codebase

We run on a Ruby on Rails 6.0.2 backend with sprinkles of Stimulus.js on the
frontend. The idea behind this stack is to keep setup and configuration simple
and to focus on dealing with the search itself.

## Getting Started

This section provides a high-level requirement & quick start guide. Follow all
steps to get up and running.

### Prerequisites

- [Ruby](https://www.ruby-lang.org/en/): we recommend using
[rbenv](https://github.com/rbenv/rbenv) to install the Ruby version listed on
the badge.
- [Yarn](https://yarnpkg.com/): please refer to their
[installation guide](https://yarnpkg.com/en/docs/install).
- [PostgreSQL](https://www.postgresql.org/) 9.4 or higher.
- [ElasticSearch](https://www.elastic.co/products/elasticsearch)
- A process manager: we recommend using
[hivemind](https://github.com/DarthSim/hivemind)

### Standard Installation

1. Make sure all the prerequisites are installed.
1. Fork or clone this repository.
1. Run `bin/setup`
1. Set up your environment variables
    - Take a look at `.env`. This file lists all the `ENV` variables we use.
    - Create a `.env.development` file. Use this file to override or customize
    values from `.env`.
1. That's it! Use `hivemind` or any other process manager to start the server.
```
$ hivemind Procfile.dev
```

### Docker Installation

1. Make sure you have [Docker](https://www.docker.com/) and
[docker-compose](https://docs.docker.com/compose/) installed.
1. Start all services with
```
$ docker-compose up
```

### Sample Data

This project comes with a very handy rake task for setting up sample data for
development. Run `bin/rails dev:prime` to get a few records populated to your
database.

## Deployment

[TODO]
