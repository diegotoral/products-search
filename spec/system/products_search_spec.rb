# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Products Search', type: :system do
  let(:brazil) { Fabricate(:brazil) }
  let(:acai) { ProductOnPage.new 'Açai' }
  let(:dragonfly_gold) { ProductOnPage.new 'Dragonfly gold necklace-choker' }
  let(:dragonfly_silver) { ProductOnPage.new 'Dragonfly silver necklace-choker' }

  before :each do
    Fabricate(:product, title: 'Dragonfly gold necklace-choker', price: 10)
    Fabricate(:product, title: 'Dragonfly silver necklace-choker', price: 100)
    Fabricate(:product, title: 'Açai', country: brazil, price: 3)

    Product.reindex
  end

  scenario 'I can see products in the page' do
    visit '/'

    expect(acai).to be_visible
    expect(dragonfly_silver).to be_visible
    expect(dragonfly_gold).to be_visible
  end

  scenario 'I can find products by word match on title', js: true do
    search = ProductSearchForm.new

    visit '/'

    search.query 'dragonfly'
    search.submit

    expect(page).to have_content 'Found about 2 results'
    expect(dragonfly_silver).to be_visible
    expect(dragonfly_gold).to be_visible
  end

  scenario 'I can filter products by country', js: true do
    search = ProductSearchForm.new
    brazil_presenter = CountryPresenter.new brazil

    visit '/'

    search.select_country(brazil_presenter.name)
    search.submit

    expect(page).to have_content 'Found about 1 result'
    expect(acai).to be_visible
  end

  scenario 'I can filter products by price', js: true do
    search = ProductSearchForm.new

    visit '/'
    search.price_less_than 10
    search.submit

    expect(page).to have_content 'Found about 2 results'
    expect(acai).to be_visible
    expect(dragonfly_gold).to be_visible
    expect(dragonfly_silver).not_to be_visible
  end

  scenario 'I can find products by partial matches on title', js: true do
    search = ProductSearchForm.new

    visit '/'

    search.query 'drag'
    search.submit

    expect(page).to have_content 'Found about 2 results'
    expect(acai).not_to be_visible
    expect(dragonfly_silver).to be_visible
    expect(dragonfly_gold).to be_visible
  end
end
