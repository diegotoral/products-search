# frozen_string_literal: true

Fabricator :product do
  country

  price { '3.00' }
  title { 'Tote Bag - Save Cows, Drink These' }
  description { 'Save Cows, Drink These Tote Bag. Dimensions: 14 x 16 inches. Material is 100% cotton' }
end
