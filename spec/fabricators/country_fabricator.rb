# frozen_string_literal: true

Fabricator :country do
  code { 'US' }
  name { 'United States' }
end

Fabricator :brazil, from: :country do
  code { 'BR' }
  name { 'Brazil' }
end
