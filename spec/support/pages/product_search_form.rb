# frozen_string_literal: true

class ProductSearchForm
  include Capybara::DSL

  def query(q)
    within form do
      fill_in 'product_search[q]', with: q
    end
  end

  def price_less_than(value)
    within form do
      select 'Is less than equal'
      fill_in 'product_search[price_value]', with: value
    end
  end

  def select_country(name)
    within form do
      select name
    end
  end

  def submit
    find('button').click
  end

  private

  def form
    @form ||= find 'form'
  end
end
