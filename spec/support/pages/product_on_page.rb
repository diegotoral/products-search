# frozen_string_literal: true

ProductOnPage = Struct.new(:title) do
  include Capybara::DSL

  def visible?
    has_css?('p.title', text: title)
  end
end
