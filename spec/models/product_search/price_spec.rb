# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ProductSearch::Price, type: :model do
  describe '#to_h' do
    it 'defaults to an empty hash when option or value is blank' do
      price = described_class.new '', ''

      expect(price.to_h).to be_empty
    end

    it 'returns the specified value as cents' do
      price = described_class.new :is_less_than, 1

      price_hash = price.to_h

      expect(price_hash[:lt]).to eq 100
    end

    it 'converts the specified name to an option if possible' do
      price = described_class.new :is_greater_than, 1

      price_hash = price.to_h

      expect(price_hash.keys).to contain_exactly :gt
    end
  end
end
