# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ProductSearch::SortBy, type: :model do
  describe '.options' do
    it 'returns all available sorting options' do
      expect(described_class.options).to eq described_class::OPTIONS_MAP.keys
    end
  end

  describe '#initialize' do
    it 'handles empty or nil kinds' do
      expect {
        described_class.new nil
      }.not_to raise_error
    end
  end

  describe '#options' do
    it 'defaults to DEFAULT_KIND when no valid kind is used' do
      sort_by = described_class.new nil

      expect(sort_by.options).to eq described_class::DEFAULT_OPTIONS
    end
  end
end
