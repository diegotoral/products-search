# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ProductSearch::Options, type: :model do
  describe '#query?' do
    it 'return whether q option is present' do
      options = described_class.new q: 'foobar'

      expect(options.query?).to be_truthy
    end

    it 'returns whether q option is missing' do
      options = described_class.new q: nil

      expect(options.query?).to be_falsy
    end
  end

  describe '#preload?' do
    it 'returns whether preload is true or false' do
      options = described_class.new preload: false

      expect(options.preload?).to be_falsy
    end
  end
end
