# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ProductSearch, type: :model do
  describe '#initialize' do
    it 'allows specify when to preload results' do
      expect(Product).to receive(:search)

      described_class.new preload: true
    end

    it 'does not preload results by default' do
      expect(Product).not_to receive(:search)

      described_class.new
    end
  end

  describe '#q' do
    it 'returns the query as specified on params' do
      search = described_class.new q: 'dragonfly'

      expect(search.q).to eq 'dragonfly'
    end
  end

  describe '#from' do
    it 'returns the country id if specified on params' do
      search = described_class.new from: 'country-id'

      expect(search.from).to eq 'country-id'
    end
  end
end
