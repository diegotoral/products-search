# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Country, type: :model do
  describe 'associations' do
    it { is_expected.to have_many(:products).dependent(:nullify) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of :name }
    it { is_expected.to validate_presence_of :slug }
    it { is_expected.to validate_presence_of :code }
  end

  context 'before validation' do
    it 'sets slug to the parameterized version of name' do
      country = described_class.new name: 'Foo Bar'

      country.validate

      expect(country.slug).to eq 'foo-bar'
    end

    it 'keeps slug unset when no name is provided' do
      country = described_class.new

      country.validate

      expect(country.slug).to be_empty
    end
  end

  describe '.import!' do
    it 'creates a country with the specified name' do
      name = 'Brazil'

      country = described_class.import!(name: name)

      expect(country.name).to eq name
    end

    it 'raises an error when name is blank' do
      expect {
        described_class.import! name: nil
      }.to raise_error ActiveRecord::RecordInvalid
    end

    it 'creates a country with code' do
      country = described_class.import! name: 'Brazil'

      expect(country.code).to eq 'BR'
    end
  end
end
