# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Country::Code do
  describe '.find_by_name!' do
    it 'returns a two letters code for the specified country' do
      code = described_class.find_by_name! 'Brazil'

      expect(code).to eq 'BR'
    end

    it 'raises error when failing to determine a code for the specified country' do
      expect {
        described_class.find_by_name! 'foobar'
      }.to raise_error described_class::ISOCodeNotFound
    end
  end
end
