# frozen_string_literal: true

RSpec.describe ApplicationPresenter, type: :presenter do
  describe '.wrap' do
    it 'returns all elements of the collection warped in presenters class' do
      collection = (1..5).to_a

      wrapped_collection = described_class.wrap collection

      expect(wrapped_collection).to all(be_a(described_class))
    end
  end

  describe '#eql?' do
    it 'returns whether target and self are the same object' do
      object = double(:object)
      presented_object = described_class.new object

      # We don't want object receiving eql? message.
      allow(object).to receive(:eql?).and_return(false)

      expect(presented_object).to eql presented_object
    end

    it 'returns whether the presented object and target are the same' do
      object = double(:object)
      presented_object = described_class.new object

      expect(presented_object).to eql object
    end
  end
end
