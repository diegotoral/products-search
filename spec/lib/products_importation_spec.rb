# frozen_string_literal: true

require 'rails_helper'
require Rails.root.join('lib', 'products_importation')

RSpec.describe ProductsImportation do
  describe '#run' do
    context 'when provided file is invalid or non existent' do
      it 'raises an error' do
        importation = described_class.new nil

        expect{ importation.run }.to raise_error described_class::FileNotFound
      end

      it 'rollbacks changes made to database' do
        file = file_fixture('empty-product-title.json')
        importation = described_class.new file

        expect{
          importation.run
        }.not_to change(Product, :count)
      rescue ActiveRecord::RecordInvalid
        nil
      end
    end

    context 'when the file is invalid or in the wrong format' do
      it 'raises an error' do
        file = file_fixture('invalid-file.yml')
        importation = described_class.new file

        expect{ importation.run }.to raise_error described_class::InvalidFileFormat
      end

      it 'rollbacks changes made to database' do
        file = file_fixture('empty-product-title.json')
        importation = described_class.new file

        expect{
          importation.run
        }.not_to change(Product, :count)
      rescue ActiveRecord::RecordInvalid
        nil
      end
    end

    context 'when file is valid' do
      it 'imports products to database' do
        file = file_fixture('product.json')
        importation = described_class.new file

        expect {
          importation.run
        }.to change(Product, :count).by 1
      end
    end
  end
end
