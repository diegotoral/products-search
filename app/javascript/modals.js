import Rails from '@rails/ujs'

class Modal {
  constructor(body, url) {
    this.el = null
    this.url = url
    this.body = body

    this.body.querySelectorAll('.modal-close').forEach(button => {
      button.addEventListener('click', this.close.apply(button))
    })
  }

  open() {
    Rails.ajax({
      type: 'get',

      url: this.url,

      success: (modalHTML, textStatus, xhr) => {
        this.el = modalHTML.body
        document.body.appendChild(this.el)

        modal.querySelectorAll('.modal-close').forEach(btn => {
          btn.addEventListener('click', e => {
            e.preventDefault()

            this.close(e)
          })
        })
      }
    })
  }

  close(event) {
    event.preventDefault()

    this.body.removeChild(this.el)
  }

  loadURL(url) {

  }
}

document.addEventListener("turbolinks:load", function() {
  const links = document.querySelectorAll("*[rel='modal:ajax']")

  links.forEach(link => {
    link.addEventListener('click', (event) => {
      const modal = new Modal(document.body, link.href)

      event.preventDefault()
      modal.open()
    })
  })
})
