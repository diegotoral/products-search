# frozen_string_literal: true

class CountryPresenter < ApplicationPresenter
  def name
    "#{flag} #{object.name}"
  end

  def flag
    code.tr('A-Z', "\u{1F1E6}-\u{1F1FF}")
  end
end
