# frozen_string_literal: true

class ProductsSearchPresenter < ApplicationPresenter
  def products
    @products ||= ProductPresenter.wrap(object.results)
  end

  def total_results
    h.pluralize object.total_count, 'result'
  end

  def price_options
    h.options_for_select(humanized_prices_options, selected: object.price_query)
  end

  def countries_options
    h.options_from_collection_for_select(countries, :id, :name, selected: selected_country)
  end

  def sort_by_options
    h.options_for_select(humanized_sort_by_options, object.sort_by)
  end

  def countries
    @countries ||= object.countries.map { |c| CountryPresenter.new c }
  end

  private

  def selected_country
    object.from
  end

  def humanized_prices_options
    ProductSearch::Price.options.map { |o| [o.to_s.humanize, o] }
  end

  def humanized_sort_by_options
    ProductSearch::SortBy.options.map { |o| [o.to_s.humanize, o] }
  end
end
