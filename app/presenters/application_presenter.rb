# frozen_string_literal: true

class ApplicationPresenter < SimpleDelegator
  def self.wrap(collection)
    collection.map { |i| new i }
  end

  def initialize(object)
    super
    @object = object
  end

  def eql?(target)
    target == self || object.eql?(target)
  end

  protected

  attr_reader :object

  def helpers
    ApplicationController.helpers
  end

  alias :h :helpers
end
