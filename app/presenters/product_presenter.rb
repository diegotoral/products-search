# frozen_string_literal: true

class ProductPresenter < ApplicationPresenter
  def price
    h.humanized_money_with_symbol object.price
  end

  def country
    @country ||= CountryPresenter.new object.country
  end

  def country_name
    country.name
  end
end
