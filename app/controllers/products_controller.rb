# frozen_string_literal: true

class ProductsController < ApplicationController
  def index
    @search = ProductSearch.new search_params.merge(
      preload: true,
      page: params[:page]
    )
    @search = ProductsSearchPresenter.new @search
    paginate_results @search.results
  end

  def show
    @product = Product.find params[:id]
    @product = ProductPresenter.new @product

    render layout: 'modal'
  end

  private

  def search_params
    params
      .require(:product_search)
      .permit(:q, :from, :price_query, :price_value, :sort_by)
  rescue ActionController::ParameterMissing
    {}
  end
end
