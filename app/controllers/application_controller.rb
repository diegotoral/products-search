# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include Pagy::Backend
  include Pagy::Frontend

  helper_method :pagy_bulma_nav

  protected

  # Sets the @pagy instance by reading configuration from a Searchkick::Results.
  def paginate_results(results)
    @pagy = Pagy.new_from_searchkick(results)
  end
end
