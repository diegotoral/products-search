# frozen_string_literal: true

class Country
  class Code
    ISOCodeNotFound = Class.new(StandardError)

    def self.find_by_name!(name)
      iso_country = ISO3166::Country.find_country_by_name name
      raise ISOCodeNotFound unless iso_country

      iso_country.alpha2
    end
  end
end
