# frozen_string_literal: true

class Country < ApplicationRecord
  has_many :products, dependent: :nullify

  before_validation :slugify_name

  validates :name, :slug, :code, presence: true

  def self.import!(name:)
    code = Country::Code.find_by_name! name
    create!(name: name, code: code)
  rescue Country::Code::ISOCodeNotFound
    raise ActiveRecord::RecordInvalid
  end

  private

  def slugify_name
    self[:slug] ||= name.to_s.parameterize
  end
end
