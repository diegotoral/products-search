# frozen_string_literal: true

class Product < ApplicationRecord
  belongs_to :country

  searchkick ProductSearch::DEFAULT_MATCH => %i[title]

  monetize :price_cents

  validates :title, :description, :price_cents, presence: true
end
