# frozen_string_literal: true

class ProductSearch
  include ActiveModel::Model

  DEFAULT_MATCH = :word_start
  DEFAULT_INCLUDES = %i[country].freeze

  attr_reader :options

  delegate :q, :from, :sort_by, :price_query, :price_value, to: :options
  delegate :total_count, to: :results

  def initialize(params = {})
    @options = Options.new params

    # Not pretty but allow us to control when results are loaded.
    results if options.preload?
  end

  def results
    @results ||= Product.search(
      query,
      where: where,
      order: order,
      page: options.page,
      per_page: options.per_page,
      includes: DEFAULT_INCLUDES,
      fields: ['title^2', 'description', 'tags'],
      match: DEFAULT_MATCH
    )
  end

  def countries
    @countries ||= Country.all
  end

  private

  def query
    return '*' unless options.query?
    options.q
  end

  def where
    { country_id: options.from, price_cents: price }.reject { |k, v| v.blank? }
  end

  def price
    Price.new(options.price_query, options.price_value).to_h
  end

  def order
    SortBy.new(sort_by).to_h
  end
end
