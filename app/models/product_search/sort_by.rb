# frozen_string_literal: true

class ProductSearch
  class SortBy
    OPTIONS_MAP = {
      relevance: { _score: :desc },
      newest: { updated_at: :desc },
      lowest_price: { price_cents: :asc },
      highest_price: { price_cents: :desc },
    }.freeze

    DEFAULT_OPTIONS = OPTIONS_MAP[:relevance]

    def self.options
      OPTIONS_MAP.keys
    end

    def initialize(name)
      @name = String(name)
      @options = OPTIONS_MAP.fetch(@name.to_sym, DEFAULT_OPTIONS)
    end

    delegate :to_h, to: :options

    attr_reader :name, :options
  end
end
