# frozen_string_literal: true

class ProductSearch
  class Price
    OPTIONS = {
      is_less_than: :lt,
      is_greater_than: :gt,
      is_less_than_equal: :lte
    }.freeze


    def self.options
      OPTIONS.keys
    end

    def initialize(name, value)
      @name = name.to_s
      @value = value
    end

    def to_h
      return {} if option.blank? || value.blank?

      { option => value_cents }.compact
    end

    private

    attr_reader :name, :value

    def value_cents
      @value.to_f * 100
    end

    def option
      OPTIONS[@name.to_sym]
    end
  end
end
