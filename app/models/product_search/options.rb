# frozen_string_literal: true

class ProductSearch
  class Options
    PER_PAGE = 20

    attr_reader :q, :page, :per_page, :from, :sort_by, :price, :preload,
                :price_query, :price_value

    def initialize(params)
      @q = params[:q]
      @from = params[:from]
      @sort_by = params[:sort_by]
      @page = params.fetch(:page, 1)
      @per_page = params.fetch(:per_page, PER_PAGE)
      @preload = params.fetch(:preload, false)
      @price_query = params[:price_query]
      @price_value = params[:price_value]
    end

    def query?
      q.present?
    end

    def preload?
      preload
    end
  end
end
