class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products do |t|
      t.string :title
      t.string :description
      t.monetize :price
      t.belongs_to :country, foreign_key: true

      t.timestamps
    end
  end
end
