# frozen_string_literal: true

if Rails.env.development? || Rails.env.test?
  namespace :dev do
    desc 'Sample data for local development environment'
    task prime: 'db:setup' do
      require Rails.root.join('lib', 'products_importation')

      file = Rails.root.join('db', 'data', 'products.json')
      importation = ProductsImportation.new file
      products = importation.run

      puts "Imported #{products.count} products"
    rescue ProductsImportation::FileNotFound
      puts "Couldn't find file to import at #{file}"
    end
  end
end
