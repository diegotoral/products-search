#[array frozen_string_literal: true

require 'pathname'

# = ProductsImportation
#
# Imports products from a JSON formatted file.
# If the file doesn't exist or is invalid then it raises errors.
#
# Note:
#   This class isn't intented to import big JSON files as it requires that the
#   entire file be loaded into memory and then parsed.
class ProductsImportation
  FileNotFound = Class.new(StandardError)
  InvalidFileFormat = Class.new(StandardError)

  def initialize(file)
    @file = Pathname.new file.to_s
    @countries = Hash.new { |hash, name| hash[name] = Country.import!(name: name) }
  end

  def run
    ensure_file_exists!

    ActiveRecord::Base.transaction do
      with_file_contents do |contents|
        contents.map do |data|
          country = find_or_create_country data['country']
          create_product data.merge('country' => country)
        end
      end
    end
  end

  private

  attr_reader :file, :countries

  def ensure_file_exists!
    raise FileNotFound, 'file not found' unless file.exist?
  end

  def with_file_contents
    contents = JSON.parse(file.read)
    yield contents if block_given?
  rescue JSON::ParserError
    raise InvalidFileFormat, 'something is wrong with this file'
  end

  def create_product(params)
    args = params.slice('title', 'description', 'price', 'country')
    args['tags'] = params['tags'].split(',').map(&:strip!).compact

    Product.create! args
  end

  def find_or_create_country(name)
    countries[name]
  end
end
