# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.3'

gem 'oj'
gem 'bootsnap', '>= 1.4.2', require: false
gem 'countries', '~> 3.0'
gem 'money-rails', '~>1.12'
gem 'rails', '~> 6.0.2', '>= 6.0.2.1'
gem 'pagy', '~> 3.7.2'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 4.1'
gem 'webpacker', '~> 4.0'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.7'
gem 'searchkick', '~> 4.2.0'

group :development, :test do
  gem 'awesome_print'
  gem 'capybara'
  gem 'dotenv-rails'
  gem 'fabrication'
  gem 'pry-byebug'
  gem 'pry-rails'
  gem 'rspec-rails', '~> 3.8'
  gem 'simplecov', require: false
  gem 'selenium-webdriver'
  gem 'shoulda-matchers'
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'rubocop-rails'
  gem 'spring'
  gem 'spring-commands-rspec', '~> 1.0.4'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
